public aspect SynchronizeThreads {

	int readthread = 0; 
    int writethread = 0;
    int N = 10;

	public void enter_read() {
		synchronized(this) {
			int countread = 0;
			while (readthread == -1 || writethread == 1){
				countread++;
				if (countread >= N){
					System.out.println("Error! Thread ended due to " + N + " times trying to execute its critical section.");
					Thread.currentThread().interrupt();
					return;
				}
				try{

					wait();
				}
				catch (InterruptedException e){
					e.printStackTrace();
				}
			}
			readthread++;	
		}
	}

	public void exit_read() {
		synchronized(this) {
			readthread--;
			if (readthread == 0)
				notify();

		}
	}

	public void enter_write() {
		synchronized(this) {
			int countwrite = 0;
			
			while (readthread != 0 || writethread !=0){
				countwrite++;
				if(countwrite >= N) {
					System.out.println("Error! Thread ended due to " + N + " times trying to execute its critical section.");
					Thread.currentThread().interrupt();
					return;
				}
				try{
					wait();
				}
				catch (InterruptedException e){
					e.printStackTrace();
				}
			}
			readthread = -1;
			writethread = 1;
		}
	}

	public void exit_write() {
		synchronized(this) {
			readthread = 0;
			writethread = 0;
			notifyAll();

		}
	}

	pointcut prepend_Called(PrependThread threadprepend, ElementsofList x) : call(void LinkedList.prepend(ElementsofList)) && this(threadprepend) && args(x);

	before(PrependThread threadprepend, ElementsofList x) : prepend_Called(threadprepend, x) {

		System.out.println("Tread trying to write: " + threadprepend.name);
		
		enter_write();

		System.out.println("+ Thread that entered successfully: " + threadprepend.name);

	}

	after(PrependThread threadprepend, ElementsofList x) : prepend_Called(threadprepend, x) {

		System.out.println("- Thread that finished writing: " + threadprepend.name);
		exit_write();
	}

	pointcut pop_Called(PopThread threadpop) : call(ElementsofList LinkedList.pop()) && this(threadpop);

	before(PopThread threadpop) : pop_Called(threadpop) {
		
		System.out.println("Tread trying to write: " + threadpop.name);
		
		enter_write();

		System.out.println(" + Thread that entered successfully: " + threadpop.name);
	}

	after(PopThread threadpop) : pop_Called(threadpop) {

		System.out.println(" - Thread that finished writing: " + threadpop.name);
		exit_write();
	}

	pointcut head_Called(HeadThread threadhead) : call(ElementsofList LinkedList.head()) && this(threadhead);

	before(HeadThread threadhead) : head_Called(threadhead) {

		System.out.println("Tread trying to read: " + threadhead.name);
		
		enter_read();

		System.out.println(" + Thread that entered successfully: " + threadhead.name);
	}

	after(HeadThread threadhead) : head_Called(threadhead) {

		System.out.println(" - Thread that finished reading: " + threadhead.name);
		exit_read();
	}
} 
