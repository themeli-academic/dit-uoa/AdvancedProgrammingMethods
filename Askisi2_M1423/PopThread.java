
public class PopThread extends MyThread {

    @Override
    public void run() {
        while (true) {

            ElementsofList header = mylist.pop();
            System.out.println("Element popped: " + header);

            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
                System.out.println("Interrupted while sleeping: " + name);
            }
        }

    }
}
