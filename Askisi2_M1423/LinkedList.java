
public class LinkedList {

    ElementsofList header;

    public LinkedList (){
        header = null;
    }

    void prepend(ElementsofList element) {
        element.next = header;
        header = element;

        printList();
        System.out.println(" ");
    }

    ElementsofList pop () {
        ElementsofList temp;
        if (header == null) {
            System.out.println("Error! List is empty.");
            return header;
        }

        temp =header;
        header = header.next;
        temp.next = null;

        printList();
        System.out.println(" ");

        return temp;
    }

    ElementsofList head (){

        return header;
    }

    void printList() {
        ElementsofList temp = header;
        while (temp != null ) {
            System.out.print(" , " + temp.data);
            temp = temp.next;
        }
    }


}
