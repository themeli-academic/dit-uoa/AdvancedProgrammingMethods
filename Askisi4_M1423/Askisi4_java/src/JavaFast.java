import java.util.ArrayList;
import java.util.Random;
import java.util.Collections;

public class JavaFast {

    public static void main(String [] args) {
        long start = 0l;

        ArrayList<First> firstArrayList = new ArrayList<First>();
        ArrayList<Second> secondArrayList = new ArrayList<Second>();

        int numberOfObjects = 10000;
        int maxIterations = 10;
        int startTimingIteration= 5;
        int numberOfIterations=0;

        while (numberOfIterations++ < maxIterations) {
        	if(numberOfIterations == startTimingIteration)  start = System.nanoTime();
            System.out.println("iteration " + numberOfIterations);


            for (int i = 0; i < numberOfObjects; ++i) {
                firstArrayList.add(new First());
            }


            int numberToDelete = numberOfObjects/2;
            ArrayList<Integer> objectsToDelete = new ArrayList<>();
            Random r = new Random();
            for(int i=0;i< numberToDelete; i++)
            {
                int idx = r.nextInt(numberOfObjects);
                if(objectsToDelete.contains(idx)) {i--; continue;}
                objectsToDelete.add(idx);  
            } 
            Collections.sort(objectsToDelete);
            Collections.reverse(objectsToDelete);


            for (int i=0;i<numberToDelete; i++) {
                int deleteHere = objectsToDelete.get(i);
                firstArrayList.remove(deleteHere);
            }





            for (int i = 0; i < firstArrayList.size(); i++) {
                secondArrayList.add(new Second());
            }
            System.out.println("firstArrayList : " + firstArrayList.size() + " secondArrayList: " + secondArrayList.size());

            for (int i = 0; i < firstArrayList.size(); i++) {
                System.out.println(" firstArrayList :" + firstArrayList.get(i).FirstElementName() + "secondArrayList :" + secondArrayList.get(i).FirstElementName());
            }

            firstArrayList.clear();
            secondArrayList.clear();

        }

        long end = System.nanoTime();
        double finaltime = ((double)(end - start) / 1000000l);
        System.out.println("Duration : " + Double.toString(finaltime) + " msec ");
        System.out.println("Average time : " + Double.toString(finaltime / ((double) numberOfIterations)) + " msec ");

    }
}


