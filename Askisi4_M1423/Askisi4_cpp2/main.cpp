#include <iostream>
#include <chrono>
#include <stack>
#include <sstream>
#include <string>
#include <algorithm>
#include <random>

using namespace std;


class MyObject
{
public:
    MyObject(int size)
    {
        content= new int[size];
        for(int i=0;i<size;i++) content[i] =i;

    }
    ~MyObject()
    {
        delete [] content;
    }
    int * content;
    string print()
    {
        return to_string(content[0]);
    }
};



int main(int argc, char ** argv) {

    int iterations = 50;
    int numberOfObjects = 10000;
    int numberToDelete = 200;
    int sizeOfObject = 1500;


    std::chrono::steady_clock::time_point started;

    int iteration=0;
    int startTimingIteration = 5;


    std::vector<MyObject *>  myObjects;

    for(int i = 0; i < numberOfObjects; i++)
    {
        myObjects.push_back(new MyObject(sizeOfObject));
    }

    for(int iteration=0;iteration < iterations; iteration++)
    {
        if(iteration == startTimingIteration)
            started = std::chrono::steady_clock::now();

        vector<int> objectsToDelete;
        std::default_random_engine r;
        std::uniform_int_distribution<> d(0,numberOfObjects);
        for(int i=0; i< numberToDelete; i++)
        {
            int idx = d(r);
            if(find(objectsToDelete.begin(),objectsToDelete.end(),idx) != objectsToDelete.end())
            {
                i--; continue;
            }
            objectsToDelete.push_back(idx);
        }

        std::sort(objectsToDelete.begin(), objectsToDelete.end());
        std::reverse(objectsToDelete.begin(), objectsToDelete.end());

        
        for(int i=0; i<numberToDelete; i++)
        {
            int deleteHere = objectsToDelete[i];
            delete  myObjects[deleteHere];

        }
        for(int i=0; i<numberToDelete; i++)
        {
            int deleteHere = objectsToDelete[i];
            myObjects.erase(myObjects.begin()  + deleteHere);

        }
        
        for(int i = 0; i < numberToDelete; i++)
        {
            myObjects.push_back(new MyObject(sizeOfObject));
        }

        for(int i=0;i<numberOfObjects;i++) cout << "Object : " << myObjects[i]->print() << endl;
    }

    

    for(int i = 0; i < numberOfObjects; i++)
    {
        delete  myObjects[numberOfObjects];
    }
    myObjects.clear();

    auto end = std::chrono::steady_clock::now();
    std :: cout << "Duration : " << std::chrono::duration_cast<std::chrono::milliseconds> (end - started).count() << " msec" <<  std::endl ;

    return 0;
}