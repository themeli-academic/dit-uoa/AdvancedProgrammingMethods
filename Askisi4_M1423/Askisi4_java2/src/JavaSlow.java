import java.util.ArrayList;
import java.util.Random;
import java.util.Collections;

public class JavaSlow {
    public static void main(String [] args)
    {
        int iterations = 50;
        int numberOfObjects = 10000;
        int numberToDelete = 200;
        int sizeOfObject = 1500;

        long started=0l;

        int startTimingIteration= 5;
        int iteration=0;

        ArrayList<MyObject> myobjects = new ArrayList<>();
        

        for (int numObj = 0; numObj < numberOfObjects; ++numObj) myobjects.add(new MyObject(sizeOfObject));

        while(++iteration <= iterations)
        {
            if(iteration == startTimingIteration)  started = System.nanoTime();

            System.out.println("iter " + iteration + " / " + iterations);


            ArrayList<Integer> objectsToDelete = new ArrayList<>();
            Random r = new Random();
            for(int i=0;i< numberToDelete; i++) objectsToDelete.add(r.nextInt(numberOfObjects));
            Collections.sort(objectsToDelete);
            Collections.reverse(objectsToDelete);

            for (int i=0;i<numberToDelete; i++) {
                int deleteHere = objectsToDelete.get(i);
                myobjects.remove(deleteHere);
            }


            System.gc();


            for (int i=0;i<numberToDelete; i++) {
                myobjects.add(new MyObject(sizeOfObject));
            }

            for(int i=0;i<numberOfObjects; ++i) System.out.println("Object " + myobjects.get(i).print());

        }

        long finished = System.nanoTime();
        System.out.println(Long.toString((finished-started)/1000000l) + " msec ");

    }

}
