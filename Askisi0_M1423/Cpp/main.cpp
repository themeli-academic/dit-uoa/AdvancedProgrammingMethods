#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <algorithm>
#include <map>

using namespace std;

int main()
{
    //read file

    ifstream reader;
    reader.open("myfile.txt");

    vector <int> firstint, secondint;

    map<int, vector <int> > elements;

    string line;
    //read file line by line
    while(reader >> line) {

        //split every line
        int delimeterposition = line.find("|");

        int el1 = stoi(line.substr(0,delimeterposition));
        int el2 =  stoi(line.substr(delimeterposition + 1));

        firstint.push_back(el1);
        secondint.push_back(el2);


        //if we search for the first element in the vector and reach its end, it means that we did not find the element
        if(elements.count(el1) == 0) {
            pair<int, vector<int> > t1 = make_pair(el1, vector <int>());
            elements.insert(t1);

        }
        if(find(elements.at(el1).begin(),elements.at(el1).end(),el2) == elements.at(el1).end())
            elements.at(el1).push_back(el2);


    } //end of while

    for(int i=0; i<firstint.size(); i++) {

        int firstelement = firstint[i];
        int secondelement = secondint[i];
        vector<int> * v = &(elements.at(firstelement));
        vector <int> :: iterator it = find(v->begin(),v->end(),secondelement);
        int in2 = distance(v->begin(), it);
        int s = v->size();

        if(s < 2){

            continue;
        }

        cout << firstelement << "|" << secondelement << "[" << in2+1 << " of " << s << "]" << endl;

    }

    reader.close();

    return 0;
}
