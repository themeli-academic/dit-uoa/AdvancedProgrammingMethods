import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.Map;
import java.util.HashMap;


public class ask0 {
    public static void main(String args[]) {

        List<String> impel=new ArrayList<>();


        //read file into stream, try-with-resources
        try (Stream<String> stream=Files.lines(Paths.get("myfile.txt"))) {

            impel=stream.collect(Collectors.toList());

        } catch (IOException e) {
            e.printStackTrace();
            return;
        }

        ArrayList <Integer> firstels=new ArrayList <Integer>();
        ArrayList <Integer> secels=new ArrayList <Integer>();

        Map <Integer, ArrayList <Integer> > elements=new HashMap <>();

        for(int i=0; i<impel.size(); i++){
            if (impel.get(i).isEmpty())
                continue;
            
            String[] ps=impel.get(i).split("\\|");
            String p1=ps[0];
            String p2=ps[1];

            int firstel=Integer.parseInt(p1);
            int secel=Integer.parseInt(p2);

            firstels.add(firstel);
            secels.add(secel);

            if(!elements.containsKey(firstel))
                elements.put(firstel, new ArrayList <Integer>());

            if(!elements.get(firstel).contains(secel)){
                elements.get(firstel).add(secel);
            }

        } //end of for

        for(int i=0; i<firstels.size(); i++){
            int item1=firstels.get(i);
            int item2=secels.get(i);
            int pos2=elements.get(item1).indexOf(item2);

            if(elements.get(item1).size()==1)
                continue;

            System.out.println(item1+"|"+item2+"["+(pos2+1)+" of "+elements.get(item1).size()+"]");
        }
    }
}
