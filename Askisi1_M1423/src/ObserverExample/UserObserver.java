package ObserverExample;

import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

public class UserObserver implements Observer{

    //Otan ginei kapoio update sthn ArrayList, tote kaleitai h sunarthsh update ka8e Observer

    @Override
   public void update (Observable listofbooks, Object updatedlist) {
        // casting
        ArrayList<String> myList = (ArrayList<String>) updatedlist;
        System.out.println("");
        System.out.println("The list contains below books: ");
        System.out.println(" ");

        for(int i=0; i<myList.size(); ++ i) {
            System.out.println(myList.get(i));
        }
    }
}
