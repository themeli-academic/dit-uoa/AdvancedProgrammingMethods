package ObserverExample;

public class Ask1_Observer {
    public static void main (String [] args) {

        //Dhmiourgia antikeimenwn kai ulopoihsh allagwn sthn ArrayList wste na elegx8ei h leitourgia twn observers/observables

        BookDataBase listofbooks = new BookDataBase();
        BookController controller1 = new BookController();
        UserObserver user1 = new UserObserver();

        listofbooks.addObserver(controller1);
        controller1.addObserver(user1);

        System.out.println("Initializing Data Base:");
        listofbooks.InitializeDataBase();

        System.out.println("Adding books");
        listofbooks.addBooks("Crime and Punishment");
        listofbooks.addBooks("The Dispossessed");
        listofbooks.addBooks("The Left Hand of Darkness");

        System.out.println("Removing book");
        listofbooks.removeBook("Pride and Prejudice");

        return;
    }
}
