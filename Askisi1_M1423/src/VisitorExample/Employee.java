package VisitorExample;

abstract class Employee {

    abstract void accept (EmployeeVisitor v);

    int years;
    String employee, position, manager;
    int basic_salary = 500;

    public int getDaysofHolidays() {

        int ndaysofholidays;

        if(years == 1){
            ndaysofholidays = 20;
        }
        else if(years == 2){
            ndaysofholidays = 21;
        }
        else if (years >= 3 && years <10) {
            ndaysofholidays = 22;
        }
        else{
            ndaysofholidays = 25;
        }
        return ndaysofholidays;
    }


    protected Employee(String employee_name, int total_years, String manager_name, String position_name) {
        employee = employee_name;
        years = total_years;
        manager = manager_name;
        position = position_name;
    }
}
