package VisitorExample;

public class Accountingemployee extends Employee {

    int bonus_accounting;

    protected Accountingemployee (String employee_name, int total_years, String manager_name, String position_name){
        super(employee_name, total_years, manager_name, position_name);
        bonus_accounting = 40;
    }

    void accept (EmployeeVisitor v) {
        v.visit(this);
    }
}
