package VisitorExample;

public class NumberDaysOffVisitor extends EmployeeVisitor {
    void visit (HRemployee hre) {
        System.out.println(" The total days off for employee " +hre.employee + "are " + hre.getDaysofHolidays());
    }
    void visit (Accountingemployee ace) {
        System.out.println(" The total days off for employee " +ace.employee + "are " + ace.getDaysofHolidays());

    }
    void visit (ITemployee ite) {
        System.out.println(" The total days off for employee " +ite.employee + "are " + ite.getDaysofHolidays());

    }
}
