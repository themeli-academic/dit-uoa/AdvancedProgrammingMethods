package VisitorExample;

public class ITemployee extends Employee{

    int bonus_it;

    protected ITemployee (String employee_name, int total_years, String manager_name, String position_name){
        super(employee_name, total_years, manager_name, position_name);
        bonus_it = 60;

    }

    void accept (EmployeeVisitor v) {
        v.visit(this);
    }
}
