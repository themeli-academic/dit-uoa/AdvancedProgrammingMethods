package VisitorExample;

public class SalaryVisitor extends EmployeeVisitor {

    void visit (HRemployee hre) {

        double salary;

        if (hre.years < 3) {
            salary = hre.basic_salary + hre.bonus_HR;
        }
        else if (hre.years >= 3 && hre.years <9) {
            salary = hre.basic_salary + hre.basic_salary*0.10*hre.years + hre.bonus_HR;
        }
        else if (hre.years >=9 && hre.years < 15) {
            salary = hre.basic_salary +hre.basic_salary*0.20*hre.years + hre.bonus_HR;
        }
        else {
            salary = hre.basic_salary +hre.basic_salary*0.30*hre.years + hre.bonus_HR;
        }

        System.out.println("The employee 's " + hre.employee + " salary is: " + salary + " euros.");
    }
    void visit (Accountingemployee ace) {
        double salary;

        if (ace.years < 3) {
            salary = ace.basic_salary + ace.bonus_accounting;
        }
        else if (ace.years >= 3 && ace.years <9) {
            salary = ace.basic_salary + ace.basic_salary*0.10*ace.years + ace.bonus_accounting;
        }
        else if (ace.years >=9 && ace.years < 15) {
            salary = ace.basic_salary + ace.basic_salary*0.20*ace.years + ace.bonus_accounting;
        }
        else {
            salary = ace.basic_salary +ace.basic_salary*0.30*ace.years + ace.bonus_accounting;
        }

        System.out.println("The employee 's " + ace.employee + " salary is: " + salary + " euros.");
    }
    void visit (ITemployee ite) {

        double salary;

        if (ite.years < 3) {
            salary = ite.basic_salary + ite.bonus_it;
        }
        else if (ite.years >= 3 && ite.years <9) {
            salary = ite.basic_salary + ite.basic_salary*0.10*ite.years + ite.bonus_it;
        }
        else if (ite.years >=9 && ite.years < 15) {
            salary = ite.basic_salary + ite.basic_salary*0.20*ite.years + ite.bonus_it;
        }
        else {
            salary = ite.basic_salary + ite.basic_salary*0.30*ite.years + ite.bonus_it;
        }

        System.out.println("The employee 's " + ite.employee + " salary is: " + salary + " euros.");

    }
}
