package VisitorExample;


public class Ask1_visitor {
    public static void main (String [] args){

        Employee hre = new HRemployee("Katerina", 10, "Nikos Anagnostou", "HR Consultant");
        Employee ace = new Accountingemployee("Anna", 10, "Marios Eleftheriou", "Administration Specialist");
        Employee ite = new ITemployee("Kostas", 10, "Christina Sotiriou", "IT Region Manager");

        EmployeeVisitor managerv = new PrintManagerVisitor();
        EmployeeVisitor descrv = new DescriptionEmployeeVisitor();
        EmployeeVisitor vacationsv = new NumberDaysOffVisitor();
        EmployeeVisitor salaryv = new SalaryVisitor();

        hre.accept(managerv);
        hre.accept(descrv);
        hre.accept(vacationsv);
        hre.accept(salaryv);

        ace.accept(managerv);
        ace.accept(descrv);
        ace.accept(vacationsv);
        ace.accept(salaryv);

        ite.accept(managerv);
        ite.accept(descrv);
        ite.accept(vacationsv);
        ite.accept(salaryv);

    }
}
